all: cve-bin-tool-output.json

# Try to auto-detect the components in the official Solr docker image
# using syft, output as cyclonedx json:
solr.docker.syft.cyclonedx.sbom.json:
	syft -o cyclonedx-json solr > $@

# Try to match the components in the official Solr docker image to CVE's.
# This does not even match the commons-text CVE-2022-42889 that is present
# (but known to be non-impacting)
cve-bin-tool-output.json: solr.docker.syft.cyclonedx.sbom.json
	@rm $@ || true
	cve-bin-tool --sbom cyclonedx --sbom-file $< -o $@ -f json
