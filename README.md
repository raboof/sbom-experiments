This repo contains some experiments around various tools for
generating SBOM's and analyzing them.

Most products of the tools used are checked in so they can
be browsed without running the tools yourself. The Makefile
should help explain how the different files relate to each
other.
